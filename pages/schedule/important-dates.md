---
name: Important Dates
---
<style type="text/css">
tr td:first-child {
  width: 10rem;
}
</style>

# Important Dates

| **MARCH**             |                                                                      |
|-----------------------|----------------------------------------------------------------------|
| 15 (Friday)           | Opening of attendee registration and requesting bursaries; Opening of Call For Proposals |


| **APRIL**             |                                                                      |
|-----------------------|----------------------------------------------------------------------|
| 15 (Monday)           | Last day to submit bursary applications                              |
| 30 (Tuesday)          | Bursary decisions posted                                             |

| **MAY**               |                                                                      |
|-----------------------|----------------------------------------------------------------------|
| 12 (Sunday)           | Last day for submitting a talk that will be considered for the official schedule             |
| 14 (Tuesday)          | Last day to accept or withdraw travel bursary (first round)          |

| **JUNE**              |                                                                      |
|-----------------------|----------------------------------------------------------------------|
| 01 (Saturday          | Last day to accept or withdraw food/accommodation bursary            |
| 14 (Friday)           | Last day to confirm your attendance                                  |

| **JULY**              |                                                                      |
|-----------------------|----------------------------------------------------------------------|
| *DebCamp*             |                                                                      |
| 14 (Sunday)           | First day of DebCamp                                                 |
| 15 (Monday)           | Second day of DebCamp                                                |
| 16 (Tuesday)          | Third day of DebCamp                                                 |
| 17 (Wednesday)        | Fourth day of DebCamp                                                |
| 18 (Thursday)         | Fifth day of DebCamp                                                 |
| 19 (Friday)           | Sixth day of DebCamp                                                 |
| *OpenDay*             |                                                                      |
| 20 (Saturday)         | Open Day; afternoon: job fair; Arrival day for DebConf, Set-up       |
| *DebConf*             |                                                                      |
| 21 (Sunday)           | First day of DebConf / opening ceremony                              |
| 22 (Monday)           | Second day of DebConf / women and diversity lunch / evening: cheese and wine party |
| 23 (Tuesday)          | Third day of DebConf                                                 |
| 24 (Wednesday)        | Fourth day of DebConf / all day: day trip                            |
| 25 (Thursday)         | Fifth day of DebConf / evening: conference dinner                    |
| 26 (Friday)           | Sixth day of DebConf                                                 |
| 27 (Saturday)         | Last day of DebConf / closing ceremony / teardown                    |
| 28 (Sunday)           | Free day / morning: visit handcraft fair / lunch brazilian barbecue  |
| 29 (Monday)           | Departure day :-(                                                    |
