---
title: Last call for bursary applications
---

If you intend to apply for a DebConf19 bursary and have not yet done so, please proceed as soon as possible!

Bursary applications for DebConf19 will be accepted **until April 15th at 23:59 UTC**.
Applications submitted after this deadline will not be considered.

You can apply for a bursary when you [register](/register/) for the conference.

Remember that giving a talk or organising an event is considered towards your bursary;
if you have a submission to make, submit it even if it is only sketched-out.
You will be able to detail it later.
DebCamp plans can be entered in the usual [Sprints page](https://wiki.debian.org/Sprints) at the Debian wiki.

Please make sure to double-check your accommodation choices (dates and venue).
Details about accommodation arrangements can be found on the [accommodation page](/about/accommodation/).
