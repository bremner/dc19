# -*- encoding: utf-8 -*-
from pathlib import Path
from datetime import date
from decimal import Decimal

from django.urls import reverse_lazy

from wafer.settings import *

TIME_ZONE = 'America/Sao_Paulo'
USE_L10N = False
TIME_FORMAT = 'G:i'
SHORT_DATE_FORMAT = 'Y-m-d'
DATETIME_FORMAT = 'Y-m-d H:i:s'
SHORT_DATETIME_FORMAT = 'Y-m-d H:i'

try:
    from localsettings import *
except ImportError:
    pass

root = Path(__file__).parent

INSTALLED_APPS = (
    'debconf',
    'dc19',
    'exports',
    'news',
    'django_countries',
    'paypal.standard.ipn',
    'bursary',
    'front_desk',
    'invoices',
    'register',
    'volunteers',
) + INSTALLED_APPS

STATIC_ROOT = str(root / 'localstatic/')

STATICFILES_DIRS = (
    str(root / 'static'),
)

STATICFILES_STORAGE = (
    'django.contrib.staticfiles.storage.ManifestStaticFilesStorage')

TEMPLATES[0]['DIRS'] = TEMPLATES[0]['DIRS'] + (
    str(root / 'templates'),
)
TEMPLATES[0]['OPTIONS']['context_processors'] += ('dc19.context_processors.expose_wafer_talks',)


WAFER_MENUS += (
    {
        'menu': 'about',
        'label': 'About',
        'items': [
            {
                'menu': 'debconf',
                'label': 'DebConf',
                'url': reverse_lazy('wafer_page', args=('about/debconf',)),
            },
            {
                'menu': 'debian',
                'label': 'Debian',
                'url': reverse_lazy('wafer_page', args=('about/debian',)),
            },
            {
                'menu': 'accommodation',
                'label': 'Accommodation',
                'url': reverse_lazy('wafer_page', args=('about/accommodation',)),
            },
            {
                'menu': 'bursaries',
                'label': 'Bursaries',
                'url': reverse_lazy('wafer_page', args=('about/bursaries',)),
            },
            {
                'menu': 'cheese-and-wine-party',
                'label': 'Cheese and Wine Party',
                'url': reverse_lazy('wafer_page', args=('about/cheese-and-wine-party',)),
            },
            {
                'menu': 'child-care',
                'label': 'Child care',
                'url': reverse_lazy('wafer_page', args=('about/childcare',)),
            },
            {
                'menu': 'confdinner',
                'label': 'Conference Dinner',
                'url': reverse_lazy('wafer_page', args=('about/confdinner',)),
            },
            {
                'menu': 'curitiba',
                'label': 'Curitiba',
                'url': reverse_lazy('wafer_page', args=('about/curitiba',)),
            },
            {
                'menu': 'faq',
                'label': 'FAQ',
                'url': 'https://wiki.debian.org/DebConf/19/Faq',
            },
            {
                'menu': 'registration_information',
                'label': 'Registration Information',
                'url': reverse_lazy('wafer_page', args=('about/registration',)),
            },
            {
                'menu': 'venue',
                'label': 'Venue',
                'url': reverse_lazy('wafer_page', args=('about/venue',)),
            },
            {
                'menu': 'visas',
                'label': 'Visas',
                'url': reverse_lazy('wafer_page', args=('about/visas',)),
            },
        ],
    },
    {
        'menu': 'sponsors_index',
        'label': 'Sponsors',
        'items': [
            {
                'menu': 'become_sponsor',
                'label': 'Become a Sponsor',
                'url': reverse_lazy('wafer_page', args=('sponsors/become-a-sponsor',)),
            },
            {
                'menu': 'sponsors',
                'label': 'Our Sponsors',
                'url': reverse_lazy('wafer_sponsors'),
            },
        ],
    },
    {
        'menu': 'schedule_index',
        'label': 'Schedule',
        'items': [
            {
                'menu': 'cfp',
                'label': 'Call for Proposals',
                'url': reverse_lazy('wafer_page', args=('cfp',)),
            },
            {
                'menu': 'confirmed_talks',
                'label': 'Confirmed Talks',
                'url': reverse_lazy('wafer_users_talks'),
            },
            {
                'menu': 'important-dates',
                'label': 'Important Dates',
                'url': reverse_lazy('wafer_page', args=('schedule/important-dates',)),
            },
            {
                'menu': 'mobile_schedule',
                'label': 'Mobile-Friendly Schedule',
                'url': reverse_lazy('wafer_page', args=('schedule/mobile',)),
            },
            {
                'menu': 'openday',
                'label': 'Open Day',
                'url': reverse_lazy('wafer_page', args=('schedule/openday',)),
            },
            {
                'menu': 'schedule',
                'label': 'Schedule',
                'url': reverse_lazy('wafer_full_schedule'),
            },
        ],
    },
    {
        'menu': 'wiki_link',
        'label': 'Wiki',
        'url': 'https://wiki.debian.org/DebConf/19',
    },
    {
        'menu': 'pt_br_index',
        'label': 'Português',
        'items': [
            {
                'menu': 'pt_br',
                'label': 'Sobre a DebConf',
                'url': reverse_lazy('wafer_page', args=('pt-br',)),
            },
            {
                'menu': 'pt_br_bolsas',
                'label': 'Bolsas',
                'url': reverse_lazy('wafer_page', args=('pt-br/bolsas',)),
            },
            {
                'menu': 'pt_br_help',
                'label': 'Como ajudar',
                'url': reverse_lazy('wafer_page', args=('pt-br/help',)),
            },
            {
                'menu': 'pt_br_creche',
                'label': 'Creche',
                'url': reverse_lazy('wafer_page', args=('pt-br/creche',)),
            },
            {
                'menu': 'pt_br_inscricao',
                'label': 'Inscrição',
                'url': reverse_lazy('wafer_page', args=('pt-br/inscricao',)),
            },
            {
                'menu': 'pt_br_openday',
                'label': 'Open Day',
                'url': reverse_lazy('wafer_page', args=('pt-br/openday',)),
            },
        ],
    },
    {
        'menu': 'contact_link',
        'label': 'Contact us',
        'url': reverse_lazy('wafer_page', args=('contact',)),
    },
)

WAFER_DYNAMIC_MENUS = ()

ROOT_URLCONF = 'urls'

CRISPY_TEMPLATE_PACK = 'bootstrap4'
CRISPY_FAIL_SILENTLY = not DEBUG

MARKITUP_FILTER = ('markdown.markdown', {
    'extensions': [
        'markdown.extensions.smarty',
        'markdown.extensions.tables',
        'markdown.extensions.toc',
        'dc19.markdown',
    ],
    'output_format': 'html5',
    'safe_mode': False,
})
MARKITUP_SET = 'markitup/sets/markdown/'
JQUERY_URL = 'js/jquery.js'

DEFAULT_FROM_EMAIL = 'registration@debconf.org'
REGISTRATION_DEFAULT_FROM_EMAIL = 'noreply@debconf.org'

WAFER_REGISTRATION_MODE = 'custom'
WAFER_USER_IS_REGISTERED = 'register.models.user_is_registered'

WAFER_VIDEO_REVIEWER = False
WAFER_PUBLIC_ATTENDEE_LIST = False

DEBCONF_CITY = 'Curitiba'
DEBCONF_NAME = 'DebConf 19'
DEBCONF_PAID_ACCOMMODATION = False
DEBCONF_CONFIRMATION_DEADLINE = date(2019, 6, 14)
DEBCONF_BURSARY_DEADLINE = date(2019, 4, 15)
DEBCONF_LOCAL_CURRENCY = 'BRL'
DEBCONF_LOCAL_CURRENCY_SYMBOL = 'R$'
DEBCONF_LOCAL_CURRENCY_RATE = Decimal('3.50')
DEBCONF_BREAKFAST = False

DEBCONF_DATES = (
    # Conference part, start date, end date
    ('DebCamp', date(2019, 7, 14), date(2019, 7, 19)),
    ('Open Day', date(2019, 7, 20), date(2019, 7, 20)),
    ('DebConf', date(2019, 7, 21), date(2019, 7, 28)),
)
DEBCONF_CONFERENCE_DINNER_DAY = date(2019, 7, 25)

VOLUNTEERS_FIRST_DAY = date(2019, 7, 12)
VOLUNTEERS_LAST_DAY = date(2019, 7, 28)

DEBCONF_T_SHIRT_SIZES = (
    ('', 'No T-shirt'),
    ('', '—'),
    ('pp', 'PP Extra Small - Straight Cut'),
    ('s', 'P Small - Straight Cut'),
    ('m', 'M Medium - Straight Cut'),
    ('g', 'G Medium-Large - Straight Cut'),
    ('gg', 'GG Large - Straight Cut'),
    ('xg', 'XG Extra Large - Straight Cut'),
    ('xxg', 'XXG Extra Large - Straight Cut'),
    ('xxxg', 'XXXG Extra Large - Straight Cut'),
    ('', '—'),
    ('s-f', 'P Small - Fitted Cut'),
    ('m-f', 'M Medium - Fitted Cut'),
    ('g-f', 'G Large - Fitted Cut'),
    ('gg-f', 'GG Extra Large - Fitted Cut'),
    ('', '—'),
    ('1-c', 'Child Size 1'),
    ('2-c', 'Child Size 2'),
    ('4-c', 'Child Size 4'),
    ('6-c', 'Child Size 6'),
    ('8-c', 'Child Size 8'),
    ('10-c', 'Child Size 10'),
    ('12-c', 'Child Size 12'),
    ('14-c', 'Child Size 14'),
    ('16-c', 'Child Size 16'),
)
DEBCONF_SHOE_SIZES = (
    ('', 'No Shoes'),
    ('33/34', 'Brazil: 33/34'),
    ('35/36', 'Brazil: 35/36'),
    ('37/38', 'Brazil: 37/38'),
    ('39/40', 'Brazil: 39/40'),
    ('41/42', 'Brazil: 41/42'),
    ('43/44', 'Brazil: 43/44'),
    ('45/46', 'Brazil: 45/46'),
    ('47/48', 'Brazil: 47/48'),
)

PAGE_DIR = '%s/' % (root / 'pages')
NEWS_DIR = '%s/' % (root / 'news' / 'stories')
SPONSORS_DIR = '%s/' % (root / 'sponsors')

TRACKS_FILE = str(root / 'tracks.yml')
TALK_TYPES_FILE = str(root / 'talk_types.yml')

BAKERY_VIEWS += (
    'dc19.views.IndexView',
    'news.views.NewsItemView',
    'news.views.NewsFeedView',
    'news.views.NewsRSSView',
    'news.views.NewsAtomView',
)
BUILD_DIR = '%s/' % (root / 'static_mirror')
