---
title: DebConf19 registration is open!
---

[Leia em Português abaixo](#debconf19-inscricoes-abertas)

<img src="{% static "img/registration-is-open.png" %}" class="img-fluid"
     alt="Registration and bursary applications are OPEN. Bursary applications must be submitted by April 15th">

Registration for [DebConf19][debconf19] is now
open. The event **will take place from July 21st to 28th, 2019 at the
Central campus of Universidade Tecnológica Federal do Paraná - UTFPR,
in Curitiba, Brazil**, and will be preceded by DebCamp, from July
14th to 19th, and an [Open Day](/schedule/openday/)
on the 20th.

[DebConf][debconf] is an event open to everyone, no matter how
you identify yourself or how others perceive you. We want to increase visibility
of our diversity and work towards inclusion at Debian Project, drawing our
attendees from people just starting their Debian journey, to seasoned Debian
Developers or active contributors in different areas like packaging,
translation, documentation, artwork, testing, specialized derivatives, user
support and many other. In other words, all are welcome.

To register for the event, log into the
[registration system](/register/)
and fill out the form.
You will be able to edit and update your registration at any
point. However, in order to help the organisers have a better estimate of
how many people will attend the event, we would appreciate if you could
access the system and confirm (or cancel) your participation in the Conference
as soon as you know if you will be able to come. **The last day to confirm or
cancel is June 14th, 2019 23:59:59 UTC**. If you don't confirm or you
register after this date, you can come to the DebConf19 but we cannot
guarantee availability of accommodation, food and swag (t-shirt, bag…).

For more information about registration, please visit
[Registration Information](/about/registration/)

## Bursary for travel, accomodation and meals

In an effort to widen the diversity of DebConf attendees,
the Debian Project allocates a part of the financial resources obtained through
sponsorships to pay for bursaries (travel, accommodation, and/or meals) for
participants who request this support when they register.

As resources are limited, we will examine the requests and decide who will
receive the bursaries. They will be destined:

 * To active Debian contributors.
 * To promote diversity: newcomers to Debian and/or DebConf, especially from
under-represented communities.

Giving a talk, organizing an event or helping during DebConf19 is taken into
account when deciding upon
your bursary, so please mention them in your bursary application.
DebCamp plans can be entered in the usual
[Sprints page at the Debian wiki][sprints].

For more information about bursaries, please visit
[Applying for a Bursary to DebConf](/about/bursaries/)

**Attention:** the registration for DebConf19 will be open until
Conference, but the **deadline to apply for bursaries using
the registration form before April 15th, 2019 23:59:59 UTC**.
This deadline is necessary in order to the organisers use time to analyze the requests,
and for successful applicants to prepare for the conference.

To register for the Conference, either with or without a bursary request, please
visit: [https://debconf19.debconf.org/register](/register/)

DebConf would not be possible without the generous support of all our
sponsors, especially our Platinum Sponsors
[**Infomaniak**][infomaniak]
and [**Google**][google].
DebConf19 is still accepting sponsors; if you are interested, or think
you know of others who would be willing to help, [please get in touch](/sponsors/become-a-sponsor/)!


This news item was originally posted in [the Debian blog][blog].

*****

# DebConf19 inscrições abertas!

<img src="{% static "img/registration-is-open.png" %}" class="img-fluid"
     alt="Registration and bursary applications are OPEN. Bursary applications must be submitted by April 15th">

Estão abertas as inscrições para a
[DebConf19][debconf19] **que acontecerá de 21 a 28 de julho
de 2019 no Campus central da Universidade Tecnológica Federal do Paraná - UTFPR,
em Curitiba - Brasil**. O evento será precedido pela DebCamp, de 14 a 19 de
julho, e pelo [Open Day](/schedule/openday/) no
dia 20 de julho.

A [DebConf][debconf] é um evento aberto a todos(as), não
importando como você se identifica ou como os(a) outros(as) o(a) percebem. Queremos
aumentar a visibilidade da nossa diversidade e trabalhar para a inclusão no
Projeto Debian, incentivando a participação desde pessoas que estão iniciando a
sua jornada no Debian até desenvolvedores(as) experientes ou
contribuidores(as) ativos(as) do Debian, em diferentes áreas como empacotamento, tradução,
documentação, design, testes, derivados especializados, suporte e muitos
outras. Em outras palavras, todos(as) são bem-vindos(as)!

Para se inscrever no evento, você deverá fazer login
[no sistema de registro](/register/) e preencher o formulário.
Você poderá editar e atualizar as suas informações a qualquer momento. No
entanto, para ajudar os organizadores a ter uma estimativa melhor de quantas
pessoas realmente virão para evento, nós agradecemos se você puder acessar o
sistema e confirmar (ou cancelar) a sua participação na Conferência assim que
souber se será mesmo possível vir. **O último dia para confirmar ou cancelar a
sua inscrição é 14 de junho de 2019 às 20:59:59 (horário de Brasília)**. Se
você não confirmar ou se inscrever após essa data, você pode vir para
a DebConf19, mas nós não poderemos garantir a disponibilidade de hospedagem,
de alimentação e do kit de participante (camiseta, bolsa, etc.).

Para mais informações sobre a inscrição, acesse:
[Informações sobre a inscrição](/pt-br/inscricao/)

## Bolsa para passagens, hospedagem e alimentação

Com o objetivo de promover a participação de mais contribuidores(as) na
DebConf, o Projeto Debian destina uma parte dos recursos financeiros obtidos
com os patrocínios do evento para pagar bolsas (passagens, hospedagem e/ou
alimentação) para os(as) participantes que solicitarem esse apoio/patrocínio
no momento da sua inscrição.

Como os recursos financeiros são limitados, nós avaliaremos os pedidos e
decidiremos quem receberão as bolsas. As bolsas são destinadas:

 * Para contribuidores(as) ativos(as) do Debian.
 * Para promoção de diversidade: novos(as) participantes no Debian e/ou na
   DebConf, especialmente de grupos sub-representados na área de tecnologia.

Palestrar, organizar uma atividade ou ajudar durante a DebConf19 conta pontos
para ganhar a bolsa, então por favor mencione essas iniciativas no seu pedido.
Os planos para a DebCamp podem ser inseridos no link habitual: 
[Página dos Sprints no wiki do Debian][sprints].

Para mais informações sobre as bolsas, acesse:
[Candidatando-se para uma Bolsa para da DebConf](/pt-br/bolsas/)

**Atenção:** as inscrições para a DebConf19 ficarão abertas até a data do
da Conferência, mas a **solicitação das bolsas deverá ser realizada pelas pessoas
interessadas usando o formulário de inscrição até o dia 15 de abril de
2019 às 20:59:59 (horário de Brasília).**
Esse prazo é necessário para que os organizadores tenham tempo
suficiente para analisar os pedidos, e para que os aplicantes
selecionados possam se preparar para a Conferência.

Para realizar a sua inscrição, com ou sem pedido de bolsa, acesse: 
[https://debconf19.debconf.org/register](/register/)

A DebConf não seria possível sem o generoso suporte de todos os
patrocinadores, especialmente dos nossos Patrocinadores
Platinum [**Infomaniak**][infomaniak]
e [**Google**][google].  A DebConf19 ainda está
aceitando patrocinadores; se você está interessado, ou acha que
conhece outras pessoas que poderiam
ajudar,
[por favor, entre em contato](/sponsors/become-a-sponsor/)!



Esta notícia foi originalmente publicada no [blog do Debian][blogpt].


[debconf19]: https://debconf19.debconf.org
[debconf]: https://www.debconf.org
[sprints]: https://wiki.debian.org/Sprints
[infomaniak]: https://www.infomaniak.com/
[google]: https://www.google.com
[sponsors]: mailto:sponsors@debconf.org
[blog]: https://bits.debian.org/2019/03/debconf19-open-registration-bursary.html
[blogpt]: https://bits.debian.org/2019/03/debconf19-open-registration-bursary-pt-BR.html
