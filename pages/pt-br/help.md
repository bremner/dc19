---
name: Ajude a organizar
---
# Ajude a organizar a DebConf19

Por ser um evento internacional, as discussões sobre a organização da DebConf
acontecem entre pessoas de várias partes do mundo e em [inglês](/contact/).

No entanto se você quer ajudar a organizar a DebConf19 e não se sente
confortável para se comunicar em inglês, você pode entrar em contato com o time
local brasileiro pela lista de discussão [debian-br-eventos@alioth-lists.debian.net][].

A lista é usada para organização geral. A inscrição é aberta e o [histórico é
público][].

Para discussão ao vivo, você pode se conectar no canal do IRC DebConf19-br (chat)
com o cliente da sua escolha: [#debconf19-br no OFTC.net][#debconf19-br] ou use
o seu navegador com um [webchat][webchat-#debconf19-br].

Não usamos grupos no Facebook, WhatsApp ou Telegram.

Onde você pode ajudar:

 * [wiki](https://wiki.debian.org/DebConf/19/TeamRoles): lista de papeis
desempenhados por cada time. [Crie o seu usuário](https://wiki.debian.org/DebConf/19/TeamRoles?action=newaccount)
para poder editar a wiki.
 * [kanboard no git](https://salsa.debian.org/debconf-team/public/data/dc19/boards):
lista de tarefas pendentes. [Crie o seu usuário](https://signup.salsa.debian.org/register/guest)
no salsa e solicite acesso ao [repositório da DebConf19](https://salsa.debian.org/debconf-team/public/data/dc19).

Elaboramos um [manual](https://salsa.debian.org/debconf-team/public/data/dc19/blob/master/manual-como-ajudar.md)
explicando com mais detalhes como usar as ferramentas de comunicação para nos
ajudar.

[#debconf19-br]: irc://irc.debian.org/debconf19-br
[webchat-#debconf19-br]: https://webchat.oftc.net/?nick=&channels=#debconf19-br
[debian-br-eventos@alioth-lists.debian.net]: mailto:debian-br-eventos@alioth-lists.debian.net
[histórico é público]: https://alioth-lists.debian.net/cgi-bin/mailman/listinfo/debian-br-eventos
