---
title: Infomaniak Platinum Sponsor of DebConf19
---

Leia em Português abaixo.

We are very pleased to announce that [**Infomaniak**][infomaniak]
has committed to support [DebConf19][debconf19] as a **Platinum sponsor**.

*"Infomaniak is proud to support the annual Debian Developers' Conference"*,
said Marc Oehler, Chief Operating Officer at Infomaniak. *"The vast majority 
of our hostings work using Debian and we share this community's values:
promoting innovation whilst ensuring that security,
transparency and user freedom remains top priority."*

Infomaniak is Switzerland's largest web-hosting company,
also offering backup and storage services, solutions for event organizers,
live-streaming and video on demand services.
It wholly owns its datacenters and all elements critical 
to the functioning of the services and products provided by the company 
(both software and hardware). 

With this commitment as Platinum Sponsor,
Infomaniak contributes to make possible our annual conference,
and directly supports the progress of Debian and Free Software
helping to strengthen the community that continues to collaborate on
Debian projects throughout the rest of the year.

Thank you very much Infomaniak, for your support of DebConf19!

## Become a sponsor too!

DebConf19 is still accepting sponsors.
Interested companies and organizations may contact the DebConf team
through [sponsors@debconf.org][sponsors], and
visit the DebConf19 website at [https://debconf19.debconf.org][debconf19].

This news item was originally posted in [the Debian blog][blog].

*****

# Infomaniak Patrocinador Platinum da DebConf19


Estamos muito felizes em anunciar que
a [**Infomaniak**][infomaniak] comprometeu-se a
apoiar a [DebConf19][debconf19] como um **patrocinador Platinum**.

*"A Infomaniak está orgulhosa de apoiar a Conferência anual dos(as)
Desenvolvedores(as) do Debian"*, disse Marc Oehler, *Chief Operating
Officer* da Infomaniak. *"A grande maioria do nosso trabalho de
hospedagem funciona utilizando o Debian, e nós compartilhamos os
valores dessa comunidade: promover inovação enquanto garantimos que a
segurança, transparência e liberdade de usuário continuem sendo
prioridades principais."*

A Infomaniak é a maior empresa de hospedagem web da Suíça, que também
oferece serviços de *backup* e de armazenamento, soluções para
organizadores de eventos, serviços de transmissão em tempo real e
vídeo sob demanda. Todos os *datacenters* e elementos críticos para o
funcionamento dos serviços e produtos oferecidos pela empresa (tanto
*software* quanto *hardware*) são de propriedade absoluta da
Infomaniak.

Com esse comprometimento como Patrocinador Platinum, a Infomaniak
contribui para tornar possível nossa conferência anual e apoia
diretamente o progresso do Debian e do Software Livre, ajudando a
fortalecer a comunidade, que continua a colaborar com projetos do
Debian durante o restante do ano.

Muito obrigado Infomaniak por apoiar a DebConf19!

## Torne-se um patrocinador também!

A DebConf19 ainda está aceitando patrocinadores. Empresas e
organizações interessadas devem entrar em contato com o time da
DebConf através
do [sponsors@debconf.org][sponsors], e visitar o
website da DebConf19
em [https://debconf19.debconf.org][debconf19].


Esta notícia foi originalmente publicada no [blog do Debian][blogpt].


[debconf19]: https://debconf19.debconf.org
[debconf]: https://www.debconf.org
[infomaniak]: https://www.infomaniak.com/
[sponsors]: mailto:sponsors@debconf.org
[blog]: https://bits.debian.org/2019/02/infomaniak-platinum-debconf19.html
[blogpt]: https://bits.debian.org/2019/02/infomaniak-platinum-debconf19-pt-BR.html

