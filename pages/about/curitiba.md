---
name: Visiting Curitiba
---
# Visiting Curitiba

Curitiba is a cosmopolitan city that receives people from all over the country.
It has a small but vibrant technology scene and a strong culture of innovation.
The city has a number of tourist attractions, both inside the city limits and
around it. There is also a strong cultural scene with attractions for all tastes
including theater, movies, music, and comedy.

Southern Brazil has a strong history of immigration during the last couple of
centuries, and Curitiba has significant immigrant communities, including German,
Italian, Japanese, Polish, Ukrainian, Syrian and Lebanese. This plurality is
reflected in a broad range of typical food available, as well as corresponding
cultural landmarks of these populations which are an integral part of the city
culture.

<video class="embed-responsive embed-responsive-4by3 mb-4" controls
       poster="{% static "video/curitiba-en.jpg" %}">
  <source src="{% static "video/curitiba-en.mp4" %}" type="video/mp4">
  <p>
    A <a href="{% static "video/curitiba-en.mp4" %}">promotial video</a>
    prepared by city hall (Prefeitura de Curitiba).
  </p>
</video>

## Geographic location

Curitiba is the capital of [Paraná State][].
It is located 100km inland, between São Paulo and Porto Alegre, where
[DebConf 4][] took place.

[Paraná State]: https://en.wikipedia.org/wiki/Paran%C3%A1_%28state%29
[DebConf 4]: https://debconf4.debconf.org

<img class="img-fluid" src="{% static "img/map-brasil-curitiba.png" %}"
     title="Map of Brazil">

## Getting to Curitiba

See: the [conference venue page](/about/venue/#getting-to-curitiba) for
details.

## Weather

July is winter in Brazil.
As the weather is drier than summer, it is unlikely to rain, and even
less likely to snow.

Curitiba is known to be the "coldest capital" in Brazil, so the weather during
DebConf will definitely not be hot.

According to the [Clima Tempo][], the average July temperature over the
last 30 years is:

[Clima Tempo]: http://www.climatempo.com.br/climatologia/271/curitiba-pr

|               |       |          |
|---------------|-------|----------|
| Avg. Minimum  | 8 °C  | 46.4 °F  |
| Avg. Maximum  | 20 °C | 68 °F    |
| Avg. Rainfall | 99 mm | 4 inches |

## Transport

### Public transport

Curitiba has a world-renowned public transportation system, using buses
rather than trains.

<img class="img-fluid" src="{% static "img/bus-station.jpg" %}"
     title="A Bus Station">

The system operates similarly to metro/subway systems, with buses
running between enclosed stations.
A network of dedicated roads for large, bi-articulated buses runs along
major arteries of the city.
Local routes are served by smaller buses, on regular roads.

Fares (4.25 BRL, about 1 USD) are paid on entering the station, and
transfers without exiting the station are free.

Timetables and live data are available on Google Maps, so you can use
that to plan your trips inside the city.
We are not aware of any Free alternatives with this data.

All buses accept the cartão transporte (bus card), which you can buy for
3 BRL from [these places][bus-cards].
Most buses also accept cash, except for the [small yellow bus][yellow-bus],
which only accepts the cartão transporte.

[bus-cards]: https://www.urbs.curitiba.pr.gov.br/PORTAL/CT_cartaoAvulso.php
[yellow-bus]: https://www.urbs.curitiba.pr.gov.br/uploads/galeriaNoticaImagens/0c6223a89a56f3dca3c1cf56e2f1681055baca94.png

<img src="{% static "img/curitiba-public-transport-map.png" %}"
     title="Public transport map" class="img-fluid">

<a href="{% static "docs/curitiba-public-transport-complete.pdf" %}">Public transport map — complete</a>

### Car rental

You can rent a car starting at ~ R$100 (~USD 33) per day.

See e.g. [rentcars.com](http://www.rentcars.com/)

### Bicycle rental

You can rent a bike starting at ~ R$ 10 per hour and R$ 50 per day.

See e.g. [KuritBike][] and [Bicicletaria Cultural][].

[KuritBike]: http://kuritbike.com/
[Bicicletaria Cultural]: https://bicicultural.com/aluguel_de_bice/

### Taxi

Curitiba has plenty of taxis and it's usually easy to get one. If you are not
close to a taxi stop the best way to get a taxi is by using 99 taxis smartphone
app  (obviously non-free, available for Android and iOS), which works like Uber
but for regular taxis.

### Uber and 99POP

Uber and 99POP (a local app) work in Curitiba.

## Fun and Free time

<img class="img-fluid" title="Tourism bus"
     src="{% static "img/curitiba-onibus-turismo.jpg" %}">

[Video from Largo da Ordem handicraft fair](https://www.youtube.com/watch?v=0B_b1TTJLywi)

### Tourist information

 * [Bus Tourism Line (in english)](http://www.curitiba.pr.gov.br/idioma/ingles/linhaturismo)
 * [Curitiba Turismo (in portuguese)](http://www.turismo.curitiba.pr.gov.br)
 * [Wikivoyage about Curitiba (in english)](https://en.wikivoyage.org/wiki/Curitiba)

### Places to visit

CNN has selected [3 sights from here](http://edition.cnn.com/2014/06/09/travel/gallery/beautiful-brazil/index.html)
among the 20 most beautiful in Brazil!

Parks / Gardens

 * [Bosque do Alemão](http://www.turismo.curitiba.pr.gov.br/fotos/bosque-alemao/152/77/)
 * Bosque de Portugual
 * Bosque do Papa
 * [Botanical Garden (Jardim Botânico)](http://www.turismo.curitiba.pr.gov.br/fotos/jardim-botanico/9/100/)
 * Barigui Park (Parque Barigui)
 * Parque dos Tropeiros
 * Parque Bacacheri
 * [Memorial Ucraniano](http://www.turismo.curitiba.pr.gov.br/fotos/memorial-ucraniano/113/60/)
 * [Parque São Lourenço](http://www.turismo.curitiba.pr.gov.br/fotos/parque-sao-lourenco/119/62/)
 * [Parque Tanguá](http://www.turismo.curitiba.pr.gov.br/fotos/parque-tangua/116/61/)
 * [Praça do Japão](http://www.turismo.curitiba.pr.gov.br/fotos/praca-do-japao/109/58/)

Museums (Museus)

 * [Alfredo Andersen](http://www.maa.pr.gov.br)
 * [Arte Sacra](http://www.fundacaoculturaldecuritiba.com.br/espacos-culturais/museu-de-arte-sacra-r-masac)
 * [Automóvel](http://www.museuautomovel.com.br)
 * Botânico Municipal
 * Casa Andrade Muricy
 * [Museu do Holocausto De Curitiba](http://www.museudoholocausto.org.br/en)
 * Egípcio — Ordem Rosa Cruz
 * [Museu de Arte Contemporânea](http://www.mac.pr.gov.br)
 * [Oscar Niemeyer — MON](http://www.museuoscarniemeyer.org.br)
 * Museu Ferroviário
 * Ucraniano

Historical sector

 * [Palace of Liberty (Paço da Liberdade)](http://www.turismo.curitiba.pr.gov.br/fotos/paco-da-liberdade/15/6)
 * Order of Largo (Largo da Ordem)

Others

 * [Municipal Market](http://www.turismo.curitiba.pr.gov.br/fotos/mercado-municipal/12/5/)
 * [Opera de Arame](http://www.turismo.curitiba.pr.gov.br/fotos/opera-de-arame/157/101/)
 * Panoramic Tower (Torre Panorâmica)

### Municipal Market

The Curitiba [Municipal Market](http://mercadomunicipaldecuritiba.com.br/) is a
traditional place to buy fruits, vegetables, desserts, coffees, beers, meat, so on.

It was created in 1958 and you can find  cheeses, wines, seasoning and sauces,
in particular pepper sauce. In Brazil craft candies are very traditional, you
can find guava preserve, pumpkin preserve, milk caramel 'dulce de leche', fig
preserve, peach preserve, so on.

The craft candies area usually made of sugar, milk or water and the fresh fruit.
The Mercado Municipal de Curitiba is close to the UTFPR.

Some examples what you can find there:

 * [General photos](https://wiki.debian.org/DebConf/19/FunFreeTime/CuritibaMunicipalMarket)
 * [Nuts](https://wiki.debian.org/DebConf/19/FunFreeTime/CuritibaMunicipalMarketNuts)
 * [Fruits](https://wiki.debian.org/DebConf/19/FunFreeTime/CuritibaMunicipalMarketFruits)
 * [Candies](https://wiki.debian.org/DebConf/19/FunFreeTime/CuritibaMunicipalMarketCandies)
 * [Peppers](https://wiki.debian.org/DebConf/19/FunFreeTime/CuritibaMunicipalMarketPeppers)
 * [Coffees](https://wiki.debian.org/DebConf/19/FunFreeTime/CuritibaMunicipalMarketCoffees)
 * [Food court (all organic)](https://wiki.debian.org/DebConf/19/FunFreeTime/CuritibaMunicipalMarketOrganics)
 * Beers

## Beer

Yes, my friends, we have many microbreweries in Curitiba! Some of them:

 * [Asgard](http://www.asgardcervejaria.com.br)
 * [Bastards Brewery](http://bastardsbrewery.com.br)
 * [Benckebier](https://www.facebook.com/www.benckebier.com.br)
 * [BierHoff](http://www.bierhoff.com.br)
 * [Bodebrown](http://www.bodebrown.com.br)
 * [Bonato](http://www.stravazachopp.com.br)
 * [Brauns Bier](https://www.facebook.com/BraunsBier)
 * [Columbus](https://cervejariacolumbus.wordpress.com)
 * [De Bora Bier](https://www.facebook.com/DeBoraBier)
 * [DUM Cervejaria](http://www.dumcervejaria.com.br)
 * [F#%ing Beer](http://www.fuckingbeer.com)
 * [GaudenBier](http://www.gaudenbier.com.br)
 * [Gobe Brew](https://www.facebook.com/gobebrew)
 * [Jokers](http://cervejariajokers.com.br)
 * [Klein](http://www.cervejariaklein.com.br)
 * [Madalosso](http://www.cervejamadalosso.com.br)
 * [Maniacs Brewing](http://maniacsbrewing.com.br)
 * [Morada Cia Etílica](http://moradaciaetilica.com.br)
 * [Ogre Beer](http://ogrebeer.com.br)
 * [Oner](http://www.cervejariaoner.com.br)
 * [Pagan](http://cervejapagan.com.br)
 * [Palta](http://www.palta.com.br)
 * [Raridade Cervejas](https://www.facebook.com/raridadecervejas)
 * [Swamp Brewing](http://www.cervejariaswamp.com)
 * [Tormenta](http://cervejatormenta.com.br)
 * [Way Beer](http://waybeer.com.br)
 * [Wensky](http://wenskybeer.com.br)

[Google My Maps of microbreweries](https://www.google.com/maps/d/viewer?mid=1b2kny5rzZZDl1w0k4dUVA77NryQ&ll=-25.42045755194187%2C-49.18878818209839&z=12)

Pubs:

 * [Hop'n Roll](http://www.hopnroll.com.br) our favourite — inspired and frequented by Jon "maddog" Hall
 * [Barbarium](http://barbarium.com.br)
 * [Cervejaria da Vila](http://cervejariadavila.com.br)
 * [Clube do Malte](http://www.clubedomalte.com.br)
 * [God Save the Beer](http://godsavethebeer.com.br)

## Tourism in Brazil

 * [Visit Brazil](http://www.visitbrasil.com/en)
     * [Health](http://www.visitbrasil.com/en/informacoes-essenciais/health.html)
     * [Security](http://www.visitbrasil.com/en/informacoes-essenciais/security.html)
     * [Emergency numbers](http://www.visitbrasil.com/en/informacoes-essenciais/emergency-numbers.html)
     * [Other key information](http://www.visitbrasil.com/en/informacoes-essenciais/?_lang=en)
 * [Accessible tourism](http://www.turismoacessivel.gov.br/)
 * [Magazine Partiu Brasil](https://issuu.com/ministeriodoturismo/docs/revista_partiu_brasil)

See also the [DebConf19 FAQ](https://wiki.debian.org/DebConf/19/Faq).
