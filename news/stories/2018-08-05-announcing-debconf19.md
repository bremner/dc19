---
title: DebConf19 dates announced at DebConf18
---

Next year, DebConf will be held in Curitiba, Brazil, from July 21st to July
28th.
For the days before DebConf the local organisers will again set up
DebCamp, a session for some intense work on improving the distribution,
from July 14th to July 19th, and Open Day on the 20th.

Debian thanks the commitment of the [sponsors](/sponsors/) supporting
DebConf19.
