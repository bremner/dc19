---
name: Accommodation
---

# Accommodation

Attendees with an [accommodation bursary](/about/bursaries/) will be
accommodated at the [Nacional Inn](https://www.nacionalinn.com.br/hotel-curitiba/nacional-inn).

The Nacional Inn is located in easy walking distance (260m) from the venue.

<i class="fa fa-map" aria-hidden="true"></i>
[Map](https://www.openstreetmap.org/directions?engine=graphhopper_foot&route=-25.43861%2C-49.26867%3B-25.43721%2C-49.26768#map=19/-25.43790/-49.26800)

The bedrooms will be shared, and can accommodate 2, 3, or 4 people.

## Self-paid accommodation

DebConf isn't providing any organised accommodation for attendees
without an accommodation bursary.
Please organize your own accommodation.

You can book a room in the same hotel as the sponsored attendees by sending an
email directly to the hotel staff: Jacyrah <adm@nacionalinn.com.br> and
Lucimara <comercial@nacionalinncuritiba.com.br>.

There is another hotel next door (the Aladdin).

## Other hotels

If you are looking for a hotel near from the venue, we suggest (in order of
proximity):

 * [Hotel Aladdin](http://www.hotelaladdin.com.br)
 * [Hostel Roma](http://hostelroma.com.br)
 * [Slaviero Conceptual](https://www.slavierohoteis.com.br/hoteis/slaviero-conceptual-rockefeller-curitiba)
 * [San Juan](https://www.sanjuanhoteis.com.br/pt-br/hoteis/detalhes/1/san-juan-executive)
 * [Victoria Villa](https://www.nacionalinn.com.br/hotel-curitiba/victoria-villa)
 * [Lizon](http://www.lizon.com.br)
 * [Nacional Inn Torres](https://www.nacionalinn.com.br/hotel-curitiba/nacional-inn-torres)
 * [Golden Park](https://www.nacionalinn.com.br/hotel-curitiba/golden-park)
 * [Ibis Budget](https://www.accorhotels.com/pt-br/hotel-5519-ibis-budget-curitiba-centro/index.shtml)
 * [Go Inn](https://new.atlanticahotels.com.br/hotel/go-inn-curitiba/848636)

If you don't see any availability on their website, we'd suggest
checking booking aggregator websites, or contacting the hotel directly.
Some of the hotels sell some of their inventory exclusively through
aggregators.

<a href="{% static "img/curitiba-hotels.png" %}" title="Hotels nearby">
  <img src="{% static "img/curitiba-hotels.png" %}" class="img-fluid">
</a>
