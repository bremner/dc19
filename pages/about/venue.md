---
name: The Conference Venue
---
# The Conference Venue

## Campus

<img class="img-fluid" src="{% static "img/utfpr-externa.jpg" %}"
     title="UTFPR Campus central">

<p style="font-size:70%;">Photo by Fernando Luiz Scherer</p>

The conference will be held inside the [Central Campus][utfpr-campus] of
[UTFPR][utfpr-en] ([Português][utfpr-pt]).

[utfpr-en]: http://portal.utfpr.edu.br/english
[utfpr-pt]: http://portal.utfpr.edu.br/
[utfpr-campus]: http://www.utfpr.edu.br/estrutura-universitaria/pro-reitorias/prorec/diretoria-de-relacoes-interinstitucionais/international-affairs-eng

UTFPR is a free public University, run by the Brazilian Federal Government.
The campus is compact, occupying a single city block, so all of the
on-campus venues will be near each other.

Centrally located in Curitiba, the campus is close to a good number of
restaurants, hotels, public transport, and shopping (including
[mall][] over the road).

[mall]: http://www.shoppingestacao.com.br/

## Address

Av. Sete de Setembro, 3165  
Rebouças  
Curitiba  
PR 80.230-901

## Wikipedia

 * [Federal University of Technology — Parana (english)](https://en.wikipedia.org/wiki/Federal_University_of_Technology_%E2%80%93_Paran%C3%A1)
 * [Universidade Tecnológica Federal do Paraná — UTFPR (português)](https://pt.wikipedia.org/wiki/Universidade_Tecnol%C3%B3gica_Federal_do_Paran%C3%A1)

## Maps

 * [Open StreetMap](https://www.openstreetmap.org/way/427929017#map=17/-25.43967/-49.26734)
 * [Google Maps](https://www.google.com/maps/place/Av.+Sete+de+Setembro,+3165+-+Rebou%C3%A7as,+Curitiba+-+PR,+80230-901/@-25.4391353,-49.2718207,17z/data=!3m1!4b1!4m5!3m4!1s0x94dce46f5aaac573:0xc0d50b8e293ae5f2!8m2!3d-25.4391353!4d-49.269632)

## Inside the Campus

 * [Building Map](http://www.utfpr.edu.br/curitiba/estrutura-universitaria/diretorias/dirgrad/derac/localizacao/mapa.png/image_view_fullscreen)
 * [Google My Maps](https://www.google.com/maps/d/viewer?mid=1agQLZiZWz4hC3IhUfliDgfTLVAvkt6II&ll=-25.43925148241007%2C-49.26880533951453&z=19)

## Rooms

 * Auditorium
 * Miniauditorium
 * BoF roooms
 * Childcare
 * Front Desk
 * Noisy hacklab
 * Quiet hacklab

## Photos and videos

 * [Accessibility](https://wiki.debconf.org/wiki/DebConf18/Bids/Curitiba/UtfprAccessibility)
 * [Area for booths](https://wiki.debconf.org/wiki/DebConf18/Bids/Curitiba/UtfprBooths)
 * [Auditorium](https://wiki.debconf.org/wiki/DebConf18/Bids/Curitiba/UtfprAuditorium)
 * [Classrooms](https://wiki.debconf.org/wiki/DebConf18/Bids/Curitiba/UtfprNormalRoom)
 * [Miniauditorium](https://wiki.debconf.org/wiki/DebConf18/Bids/Curitiba/UtfprMiniAuditorium)
 * [Open garden (entrances to auditorium, miniauditorium, video-conference room)](https://wiki.debconf.org/wiki/DebConf18/Bids/Curitiba/UtfprGarden)
 * [Restaurant/cafeteria/snack bar](https://wiki.debconf.org/wiki/DebConf18/Bids/Curitiba/UtfprRestaurante)
 * [Video-conference room](https://wiki.debconf.org/wiki/DebConf18/Bids/Curitiba/UtfprVideoConferencia)

 * [Videos recorded to show the hotel, the venue and the mall](http://hemingway.softwarelivre.org/videos-diversos/curitiba)

## Getting to Curitiba

Most international flights to Brazil arrive in either São Paulo or Rio de
Janeiro, connecting to Curitiba on a local airline such as
[Gol][], [Latam][], or [Azul][].
You should be able to book a ticket directly to Curitiba (CWB),
connecting through either of these cities.

[Gol]: https://voegol.com.br/
[Latam]: https://www.latam.com/
[Azul]: http://voeazul.com.br/

### By plane from São Paulo

The travel time between São Paulo – Guarulhos International Airport (GRU) and
Curitiba's Afonso Pena International Airport (CWB) is 1h05min.

### By bus from São Paulo

From São Paulo to Curitiba it is 6 hours by bus.
It is safe, comfortable, and convenient.
Several companies operate on this route, including [Viação Cometa][],
and [Kaissara][].
Some of these services are sleeper buses.

[Viação Cometa]: http://www.viacaocometa.com.br/en
[Kaissara]: https://www.kaissara.com.br/

## Getting to the venue from the Airport

Curitiba "[International Airport Afonso Pena](https://www.aeroportocuritiba.net/en)"
(CWB) is located 18km from the city centre, in São José dos Pinhais.

It takes about an hour to get from the airport to the venue  by public
transport (bus), or 20 minutes by taxi.

### Public Transit (bus)

This is the cheapest option (R$ 4.25, about 1 USD).

The airport bus (line E32, grey), runs from the Airport to the Terminal
Boqueirão.

Transfer (for free) onto the Bi-articulado Boqueirão to the UTFPR bus
station, outside the campus.

The entrance is on Av. Sete de Setembro, just around the corner.

### Taxi

The cost is around R$ 70 (about 17 USD).
There is only one company and the cars are blue and white.

### Coach

The cost is R$ 15 (about 4 USD).
It is a good idea you say to the driver you want to get off at the
Shopping Estação bus stop.
The campus is one block west of the shopping centre.

More information: [Airport Executive](https://www.aeroportoexecutivo.com.br)

### App

You can use Uber or 99POP (99 works with particular car or taxi).

## Health

### Vaccination

Your home country may recommend vaccination before travelling to Brazil.
For example the [United States'][us-vaccines] and
[Canada's][ca-vaccines] advice are to vaccinate against:

[us-vaccines]: https://wwwnc.cdc.gov/travel/destinations/traveler/none/brazil
[ca-vaccines]: https://travel.gc.ca/destinations/brazil#health

* Hepatitis A
* Typhoid
* Measles
* [Yellow fever](https://www.who.int/csr/disease/yellowfev/en/)

There is an ongoing yellow fever outbreak in Brazil, including Paraná,
where DebConf19 will be held.
[Map of affected areas][epidemic-map].
This mostly affects rural areas, Curitiba itself (and the majority of
urban areas in Brazil) are not high risk areas for Yellow Fever.

Brazil does not require a vaccination, but the infection is life
threatening when you get bitten by a mosquito carrying the virus.
Your home country may not let you return from yellow fever epidemic
areas, such as Brazil, without a vaccination.

We therefore recommend to get a vaccination at least 10 days before
travelling to Brazil.
There is a global shortage of the vaccine, so you may need to schedule
this far in advance.

[epidemic-map]: https://ecdc.europa.eu/sites/portal/files/images/Brazil-Yellow-fever-affected-states-31012018.png

