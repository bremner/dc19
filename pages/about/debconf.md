---
name: About DebConf
---
# About DebConf

[Em português](/pt-br/)

DebConf is the annual conference for [Debian](http://debian.org) contributors
and users interested in [improving Debian](https://www.debian.org/intro/help).
[Previous Debian conferences](https://www.debconf.org) have featured speakers
and attendees from all around the world. [DebConf18][] took place in Hsinchu,
Taiwan and was attended by 298 participants from 44 countries.

[DebConf18]: https://debconf18.debconf.org/

**DebConf19 is taking place in Curitiba, Brazil from July 21st to 28th, 2019.**

It is being preceded by DebCamp (July 14th to 19th), and
[Open Day](/schedule/openday/) (July 20th).

We look forward to seeing you in Curitiba!

<form method="POST" action="https://lists.debian.org/cgi-bin/subscribe.pl">
  <fieldset>
    <legend>Register to the debconf-announce mailing list</legend>
    <label for="user_email">Your email address:</label>
    <input name="user_email" size="40" value="" type="Text">
    <input type="hidden" name="list" value="debconf-announce">
    <input name="action" value="Subscribe" type="Submit">
  </fieldset>
</form>

## The Venue

The [Federal University of Technology — Paraná (Universidade Tecnológica Federal do Paraná — UTFPR)][utfpr],
is a historic building located in central Curitiba.
The [venue](../venue/) offers several conference halls and classrooms for
plenaries, presentations, discussions, and informal workshops.
[Accommodation](../accommodation/) for participants is available in
hotels nearby.

[utfpr]: http://portal.utfpr.edu.br/english

## Codes of Conduct and Anti-harassment

DebConf is committed to a safe environment for all participants.
All attendees are expected to treat all people and facilities with respect
and help create a welcoming environment.
If you notice behaviour that fails to meet this standard, please speak up and
help to keep DebConf as respectful as we expect it to be.

If you are harassed and requests to stop are not successful, or notice a
disrespectful environment, the organizers want to help.
Please contact us at [antiharassment@debian.org][].
We will treat your request with dignity and confidentiality, investigate, and
take whatever actions appropriate. We can provide information on security,
emergency services, transportation, alternative accommodations, or whatever
else may be necessary.
If mediation is not successful, DebConf reserves the right to to take action
against those who do not cease unacceptable behaviour.

See the [DebConf Code of Conduct](https://debconf.org/codeofconduct.shtml) and
the [Debian Code of Conduct](https://www.debian.org/code_of_conduct).

[antiharassment@debian.org]: mailto:antiharassment@debian.org
