---
title: Google Platinum Sponsor of DebConf19
---

We are very pleased to announce that [**Google**][google]
has committed to support [DebConf19][debconf19] as a **Platinum sponsor**.

*"The annual DebConf is an important part of the Debian development ecosystem
and Google is delighted to return as a sponsor in support of the work
of the global community of volunteers who make Debian and DebConf a reality"*
said Cat Allman, Program Manager in the Open Source Programs
and Making & Science teams at Google.

Google is one of the largest technology companies in the
world, providing a wide range of Internet-related services and products
as online advertising technologies, search, cloud computing, software, and hardware.

Google has been supporting Debian by sponsoring DebConf since more than
ten years, and is also a Debian partner sponsoring parts 
of [Salsa][salsa]'s continuous integration infrastructure
within Google Cloud Platform.


With this additional commitment as Platinum Sponsor for DebConf19,
Google contributes to make possible our annual conference,
and directly supports the progress of Debian and Free Software
helping to strengthen the community that continues to collaborate on
Debian projects throughout the rest of the year.

Thank you very much Google, for your support of DebConf19!

## Become a sponsor too!

DebConf19 is still accepting sponsors.
Interested companies and organizations may contact the DebConf team
through [sponsors@debconf.org][sponsors], and
visit the DebConf19 website at [https://debconf19.debconf.org][debconf19].

This news item was originally posted in [the Debian blog][blog].

[debconf19]: https://debconf19.debconf.org
[debconf]: https://www.debconf.org
[google]: https://www.google.com
[salsa]: https://salsa.debian.org
[sponsors]: mailto:sponsors@debconf.org
[blog]: https://bits.debian.org/2019/03/google-platinum-debconf19.html
